This is a gitlab CI/CD based PROJECT to deploy multiple applications in the same K8s cluster.
Inth is project we will use private GKE which is a google cloud based kubernetes cluster cluster.
after finish the deploy stages of this project your website will be available on the internet like this : 
 
```
projectname.yourdns.com/branchename

```
like that you can Deploy multiple websites in the same kubernetes cluster. 

# Stages

The CI of this project have 8 stages:

- build_APP
- trigger_build_gke
- deploy_ingress_controller
- configure_DNS
- deploy_ingress_ressource
- deploy
- destroy_BRANCHE_ressources
- trigger_destroy_gke

## The first stage build_APP

in this stage we are going to build the docker image and push it to dockerhub, for this we used script called build_app.sh
this script will apply docker command to do this task.

## The second stage trigger_build_gke

this stage will launch a tigged pipeline, this pipeline will run a terraform code.
the main objective of this terraform code is to connect to google cloud account using given service account and create GKE : google kubernetes engine.
the state of this project is managed using google cloud bucket.

the trigged pipeline have 2 stages : 

* the first is create GKE : will be used when running this stage.
* the second stage: will remove the GKE

Remarque: note that the GKE is private so to be able to connect a private GKE to the internet we need to create VPC/NAT  and other ressources.
more detail of all the ressources created will be explained in the project.

project link : https://gitlab.infraplus.net/exploitation/k8s-group/gke-terraform.

Note that the GKE will be created one time and will be re-cearted only if it was deleted.

## Stage deploy_ingress_controller

The ingress controller acts as a reverse proxy and load balancer. It implements a Kubernetes Ingress. The ingress controller adds a layer of abstraction to traffic routing, accepting traffic from outside the Kubernetes platform and load balancing it to Pods running inside the platform.
we will use helm chart to deploy ingress controller.
script: deploy_ingress_controller.sh
a loadbalancer service type will be created (will created EXTERNAL IP address) and this ip address will be used in the next stage to configure DNS.

## Stage configure_DNS

This stage will run terraform code take in parameters the PROJECT name for the DNS entry and IP address.
The terraform code is the configure_DNS_terraform folder, comments explain theterraform in the .tf files.


## Stage Deploy ingress ressources

ingress ressource is the place where we can add rules to deploy more websites.
this stage will deploy the default ingress ressource ( ingress.yml ) and rules will be added to this ingress ressouce in the next stage

## Stage deploy

in this stage we will deploy the application in the kubernetes cluster using  deploy.sh script.
2 ressources will be added : the first is the deployment and the second is a clusterIP service type to be used in the ingress ressouce.
and update the ingress ressource role.
more details about this stage in the script.

## Stage destroy_BRANCHE_ressources
 
This stage will delete the ressouces related to the actual branche from the cluster. and also remove the added ingress rule.

## Stage trigger_destroy_gke

this stage wil ldestroy the hole GKE cluster
