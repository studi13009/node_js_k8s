# terraform managed state using google cloud bucket
terraform {
 backend "gcs" {
   bucket  = "dns_backet"
   prefix  = "terraform/state"
   credentials = "key.json"
 }
} 