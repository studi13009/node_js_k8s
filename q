stages:
- build_APP
- trigger_build_gke
- deploy_ingress_controller
- configure_DNS
- deploy_ingress_ressource
- deploy
- destroy_BRANCHE_ressources
- trigger_destroy_gke
###############################
## Build Docker image         #
###############################
build_APP:
  image: docker:dind
  stage: build_APP
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_REF_NAME =~ /^feature/'
      when: on_succes
    - when: never
  before_script:
    - docker login -u alolou -p '71365809Samah@'
    - chmod +x build_app.sh
  script:
    - ./build_app.sh
##############################
# trigger build private GKE  #
##############################    
trigger_build:
  stage: trigger_build_gke
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_REF_NAME =~ /^feature/'
      when: on_succes
    - when: never
  variables:
    PROJECT_NAME: $CI_PROJECT_NAME
    OBJECTIVE: "BUILD"
  trigger:
    project: exploitation/k8s-group/gke-terraform
    strategy: depend    
##############################
#  NGINX Ingress Controller  #
##############################
deploy_ingress_controller:
  image: soon/gke-helm:v3.9.0
  stage: deploy_ingress_controller
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_REF_NAME =~ /^feature/'
      when: on_succes
    - when: never
  before_script:
    - chmod +x deploy_ingress_controller.sh
    - apk update && apk add jq
  script:
    - ./deploy_ingress_controller.sh
    - MY_IP="$(kubectl get svc nginx-ingress-ingress-nginx-controller -o jsonpath='{.status.loadBalancer.ingress[0]}'  | jq -r '.ip')"
    - echo $MY_IP
    - echo "EXTERNEL_IP=$MY_IP" > ip.env
  artifacts:
    paths:
      - ip.env   
##############################
#    configure DNS           #
##############################
configure_DNS:
  stage: configure_DNS
  image: 
     name: hashicorp/terraform:light
     entrypoint: [""]
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_REF_NAME =~ /^feature/'
      when: on_succes
    - when: never
  script:
    - source ip.env
    - sed -i "s|##IP##|$EXTERNEL_IP|g" configure_DNS_terraform/main.tf
    - sed -i "s|##PROJECT##|$CI_PROJECT_NAME|g" configure_DNS_terraform/main.tf
    - cd configure_DNS_terraform
    - terraform init
    - terraform apply -auto-approve
###############################
##  Deploy ingress ressources #
###############################   
deploy_ingress_ressource:
  stage: deploy_ingress_ressource
  image: google/cloud-sdk:latest
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_REF_NAME =~ /^feature/'
      when: on_succes
    - when: never
  before_script:
    - apt update
    - chmod +x deploy_ingress_ressource.sh    
  script:
    - ./deploy_ingress_ressource.sh 
###############################
#   Deploy app on GKE         #
###############################   
deploy:
  stage: deploy
  image: google/cloud-sdk:latest
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_REF_NAME =~ /^feature/'
      when: on_succes
    - when: never
  before_script:
    - apt update
    - chmod +x deploy.sh
  script:
    - ./deploy.sh
  tags:
###############################
#  Destroy BRANCHE  Ressources#
###############################    
destroy_BRANCHE:
  image: google/cloud-sdk:latest
  stage: destroy_BRANCHE_ressources
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_REF_NAME =~ /^feature/'
      when: manual
    - when: never
  before_script:
    - chmod +x destroy_branche.sh
  script:
    - ./destroy_branche.sh
###############################
##  Destroy GKE               #
###############################    
trigger:
  stage: trigger_destroy_gke
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_REF_NAME =~ /^feature/'
      when: manual
    - when: never
  variables:
    PROJECT_NAME: $CI_PROJECT_NAME
    OBJECTIVE: "DESTROY"
  trigger:
    project: exploitation/k8s-group/gke-terraform
    strategy: depend
